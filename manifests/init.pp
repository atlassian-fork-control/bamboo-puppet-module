# Install and configure Bamboo server
class bamboo (
  String $group,
  String $gid,
  String $user,
  String $uid,
  Stdlib::Absolutepath $install_dir,
  Stdlib::Absolutepath $data_dir,
  Optional[Stdlib::Absolutepath] $environment_file = undef,
  String $version,
  String $license,
  Struct[{
    instance_name      => String,
    base_url           => String,
    config_dir         => Stdlib::Absolutepath,
    build_dir          => Stdlib::Absolutepath,
    build_working_dir  => Stdlib::Absolutepath,
    artifacts_dir      => Stdlib::Absolutepath,
    repository_log_dir => Stdlib::Absolutepath,
    broker_url         => String,
    broker_client_url  => String,
  }] $configuration,
  Struct[{
    type      => Enum['postgresql', 'oracle12c','mysql', 'mssql', 'hsqldb', 'other'],
    driver    => Optional[String],
    dialect   => Optional[String],
    url       => String,
    username  => String,
    password  => String,
    overwrite => Boolean,
  }] $database,
  Struct[{
    username  => Pattern[/^[a-z0-9]+$/],
    password  => String,
    full_name => String,
    email     => String,
  }] $administrator,
  Stdlib::Httpurl $archive_url = "https://downloads.atlassian.com/software/bamboo/downloads/atlassian-bamboo-${version}.tar.gz",
) {
  if $bamboo::database['driver'] == undef {
    # Default dialects based on ${bamboo::install_dir}/atlassian-bamboo/WEB-INF/classes/database-defaults/
    $database_driver = $bamboo::database['type'] ? {
      'postgresql' => 'org.postgresql.Driver',
      'oracle12c'  => 'oracle.jdbc.OracleDriver',
      'mysql'      => 'com.mysql.jdbc.Driver',
      'mssql'      => 'com.microsoft.sqlserver.jdbc.SQLServerDriver',
      'hsqldb'     => 'org.hsqldb.jdbcDriver',
      default      => fail("Can't determine the driver for database ${bamboo::database['type']}")
    }
  } else {
    $database_driver = $bamboo::database['driver']
  }

  if $bamboo::database['dialect'] == undef {
    # Default dialects based on ${bamboo::install_dir}/atlassian-bamboo/WEB-INF/classes/database-defaults/
    $database_dialect = $bamboo::database['type'] ? {
      'postgresql' => 'org.hibernate.dialect.PostgreSQL82Dialect',
      'oracle12c'  => 'org.hibernate.dialect.Oracle12cDialect',
      'mysql'      => 'org.hibernate.dialect.MySQL5InnoDBDialect',
      'mssql'      => 'com.atlassian.bamboo.hibernate.SQLServerIntlDialect',
      'hsqldb'     => 'org.hibernate.dialect.HSQLDialect',
      default      => fail("Can't determine the dialect for database ${bamboo::database['type']}")
    }
  } else {
    $database_dialect = $bamboo::database['dialect']
  }

  File {
    owner => $bamboo::user,
    group => $bamboo::group,
  }

  class {'bamboo::install': }
  -> class {'bamboo::config': }
  -> class {'bamboo::service': }
  -> class {'bamboo::setup': }
  -> Class['bamboo']
}
